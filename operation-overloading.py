

import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    
    def move(self, x, y):
        self.x += x
        self.y += y
    
    #! OVERLOADING THE ADD OPERATOR
    #* for instance, if we have p1 + p2, p1 becomes 'self' and p2 becomes 'p'
    #* we are creating a new point from this and returning it
    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)
    
    #! OVERLOAD THE SUBTRACT OPERATOR
    def __sub__(self, p):
        return Point(self.x - p.x, self.y - p.y)
    
    #! OVERLOAD THE MULTIPLICATION OPERATOR
    def __mul__(self, p):
        return self.x * p.x + self.y * p.y
    
    #* retuns the length of the coordinate from 0,0
    def length(self):
        return math.sqrt(self.x**2 + self.y**2)
    
    def __len__(self):
        return math.sqrt(self.x**2 + self.y**2)
    
    #* greater than
    def __gt__(self, p):
        return self.length() > p.length()
    
    #* greater than or equal to
    def __ge__(self, p):
        return self.length() >= p.length()
    
    #* less than
    def __lt__(self, p):
        return self.length() < p.length()
    
    #* less than or equal to
    def __le__(self, p):
        return self.length() <= p.length()
    
    #* equal to
    def __eq__(self, p):
        return self.x == p.x and self.y == p.y
    
    #* when we try to print our object, this is automatically called
    #* can return anything you want as long as it's a string
    def __str__(self):
        return '(' + str(self.x) + ',' + str(self.y) + ')'
         
    
   
        


if __name__ == '__main__':
    
    p1 = Point(2,3)
    p2 = Point(7,5)

    p1.move(2,3)

    p3 = p1 + p2
    p4 = p1 - p2
    p5 = p3 * p4

    print(p3, p4, p5)
    print(p1 == p2)
    print(p2 > p1)

        
    
        