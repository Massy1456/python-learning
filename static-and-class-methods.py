from unicodedata import name


class Human:
    
    humans = []
    
    def __init__(self, name):
        self.name = name
        self.humans.append(self)
        
    @classmethod
    def num_of_humans(cls):
        return len(cls.humans)
    
    @staticmethod
    def sayhello(n):
        for i in range(n):
            print('hello')
            
human = Human('Max')

Human.sayhello(5)
print(Human.num_of_humans)